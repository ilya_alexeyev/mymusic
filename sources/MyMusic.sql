CREATE TABLE album(
	album_id			INTEGER		PRIMARY KEY		AUTOINCREMENT,
	album_number		TEXT		NULL,
	label				INTEGER		NOT NULL,
	album_type			INTEGER		NOT NULL,
	recording_year		TEXT		NULL,
	release_year		TEXT		NULL,
	cloud_storage		INTEGER		NOT NULL,
	FOREIGN KEY(label)
		REFERENCES label(label_id),
	FOREIGN KEY(album_type)
		REFERENCES album_type(album_type_id)	
);

CREATE TABLE label(
	label_id			INTEGER		PRIMARY KEY		AUTOINCREMENT,
	label_name			TEXT		NOT NULL
);

CREATE TABLE composer_in_album(
	album_composer_id	INTEGER		PRIMARY KEY		AUTOINCREMENT,
	album				INTEGER		NOT NULL,
	composer			INTEGER		NOT NULL,
	FOREIGN KEY(album)
		REFERENCES album(album_id),
	FOREIGN KEY(composer)
		REFERENCES composer(composer_id)
);

CREATE TABLE composer(
	composer_id			INTEGER		PRIMARY KEY		AUTOINCREMENT,
	composer_name		TEXT		NOT NULL,
	composer_country	INTEGER		NOT NULL,
	FOREIGN KEY(composer_country)
		REFERENCES country(country_id)
);

CREATE TABLE performer_in_album(
	album_performer_id	INTEGER		PRIMARY KEY		AUTOINCREMENT,
	album				INTEGER		NOT NULL,
	performer			INTEGER		NOT NULL,
	FOREIGN KEY(album)
		REFERENCES album(album_id),
	FOREIGN KEY(performer)
		REFERENCES performer(performer_id)
);

CREATE TABLE performer(
	performer_id		INTEGER		PRIMARY KEY		AUTOINCREMENT,
	performer_name		TEXT		NOT NULL,
	performer_type		INTEGER		NOT NULL,
	performer_country	INTEGER		NOT NULL,
	FOREIGN KEY(performer_type)
		REFERENCES performer_type(performer_type_id),
	FOREIGN KEY(performer_country)
		REFERENCES country(country_id)	
);

CREATE TABLE performer_type(
	performer_type_id	INTEGER		PRIMARY KEY		AUTOINCREMENT,
	performer_type_name	TEXT		NOT NULL
);

CREATE TABLE album_type(
	album_type_id		INTEGER		PRIMARY KEY		AUTOINCREMENT,
	album_type_name		TEXT		NOT NULL
);

CREATE TABLE country(
	country_id			INTEGER		PRIMARY KEY		AUTOINCREMENT,
	country_name		TEXT		NOT NULL
);